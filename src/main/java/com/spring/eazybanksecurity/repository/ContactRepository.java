package com.spring.eazybanksecurity.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.spring.eazybanksecurity.model.Contact;

@Repository
public interface ContactRepository extends CrudRepository<Contact, Long> {


}
